import 'package:tp_flutter/models/habitation.dart';
import 'package:tp_flutter/models/habitations_data.dart';
import 'package:tp_flutter/models/type_habitat.dart';

import '../models/type_habitat_data.dart';

class HabitationService {

  final _typehabitats = TypeHabitatData.buildList();
  final _habitations = HabitationsData.buildList();

  List<TypeHabitat> getTypeHabitats() {
    return _typehabitats;
  }

  List<Habitation> getTypeHabitationsTop10() {
    return _habitations
        .where((element) => element.id%2 == 1)
        .take(10)
        .toList();
  }

  List<Habitation> getMaisons() {
    return _getHabitations(isHouse: true);
  }

  List<Habitation> getAppartements() {
    return _getHabitations(isHouse: false);
  }

  List<Habitation> _getHabitations({bool isHouse = true}) {
    return _habitations
        .where((element) => element.typeHabitat.id == (isHouse ? 1 : 2))
        .toList();
  }

}