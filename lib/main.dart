import 'dart:html';

import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:intl/intl.dart';
import 'package:tp_flutter/models/habitation.dart';
import 'package:tp_flutter/models/type_habitat.dart';
import 'package:tp_flutter/services/habitation_service.dart';
import 'package:tp_flutter/share/location_style.dart';
import 'package:tp_flutter/share/location_text_style.dart';
import 'package:tp_flutter/views/habitation_list.dart';
import 'package:tp_flutter/views/location_list.dart';
import 'package:tp_flutter/views/login_page.dart';
import 'package:tp_flutter/views/profil.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Locations',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Mes Locations'),
      routes: {
        Profil.routeName: (context) => Profil(),
        LoginPage.routeName:  (context) => const LoginPage('/'),
        LocationList.routeName: (context) => const LocationList(),
        //ValidationLocation.routeName : (context) => const ValidationLocation(),
      },
      localizationsDelegates: const [GlobalMaterialLocalizations.delegate],
      supportedLocales: const [Locale('en'), Locale('fr')],
    );
  }
}

class MyHomePage extends StatelessWidget {

  final HabitationService service = HabitationService();
  final String title;
  late List<TypeHabitat> _typehabitats;
  late List<Habitation> _habitations;

  MyHomePage({required this.title, Key? key}) : super(key: key) {

    _typehabitats = service.getTypeHabitats();
    _habitations = service.getTypeHabitationsTop10();

  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: Center(
        child: Column(
          children: [
            SizedBox(height: 30),
            _buildTypeHabitat(context),
            SizedBox(height: 30),
            _buildDerniereLocation(context)
          ],
        ),
      ),
    );
  }

  _buildTypeHabitat(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(6.0),
      height: 100,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: List.generate(
            _typehabitats.length,
                (index) => _buildHabitat(context, _typehabitats[index])
        ),
      ),
    );
  }

  _buildHabitat(BuildContext context, TypeHabitat typeHabitat) {
    var icon = Icons.house;
    switch (typeHabitat.id) {
    // case id = 1 is a House, so no need to touch the value of icon
      case 2: Icons.apartment;
      break;
      default:
        icon = Icons.home;
    }
    return Expanded(
        child: Container(
          height: 80,
          decoration: BoxDecoration(
            color: LocationStyle.backgroundColorPurple,
            borderRadius: BorderRadius.circular(8.0),
          ),
          margin: EdgeInsets.all(8.0),
          child: InkWell(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) =>
                        HabitationList(typeHabitat.id == 1),
                  )
              );
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Icon(
                  icon,
                  color: Colors.white70,
                ),
                SizedBox(width: 5),
                Text(
                  typeHabitat.libelle,
                  style: LocationTextStyle.regularWhiteTextStyle,
                )
              ],
            ),
          ),
        )
    );

  }

  _buildDerniereLocation(BuildContext context) {
    return Container(
      height: 240,
      child: ListView.builder(
          itemCount: _habitations.length,
          itemExtent: 220,
          itemBuilder: (context, index) =>
              _buildRow(
                  _habitations[index],
                  context
              ),
          scrollDirection: Axis.horizontal
      ),
    );
  }

  _buildRow(Habitation habitation, BuildContext context) {
    var format = NumberFormat("### €");

    return Container(
      width: 240,
      margin: EdgeInsets.all(4.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(20.0),
            child: Image.asset(
              'assets/images/locations/${habitation.image}',
              fit: BoxFit.fitWidth,
            ),
          ),
          Text(
            habitation.libelle,
            style: LocationTextStyle.regularTextStyle,
          ),
          Row(
            children: [
              Icon(Icons.location_on_outlined),
              Text(
                habitation.adresse,
                style: LocationTextStyle.regularTextStyle,
              ),
            ],
          ),
          Text(
            format.format(habitation.prixmois),
            style: LocationTextStyle.boldTextStyle,
          ),
        ],
      ),
    );
  }
}


