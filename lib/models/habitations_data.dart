import 'habitation.dart';

class HabitationsData {
  static final data = [
    {
      'id': 1,
      'typehabitat': {'id': 1, 'libelle': "Maison"},
      'libelle': "Maison moderne à la campagne",
      'image': "maison.png",
      'adresse': "123 Main St, Campagne, USA",
      'habitantsMax': 6,
      'chambres': 3,
      'lits': 6,
      'sdb': 2,
      'superficie': 200,
      'prixmois': 1000,
      'options': [
        {'id': 1, 'libelle': "Piscine", 'description': "Piscine privée"},
        {'id': 2, 'libelle': "Jardin", 'description': "Jardin privé"},
      ],
      'optionPayantes': [
        {'id': 1, 'libelle': "Parking", 'description': "Parking privé", 'prix': 100},
        {'id': 2, 'libelle': "Cleaning", 'description': "Service de nettoyage", 'prix': 200},
      ],
    },
    {
      'id': 2,
      'typehabitat': {'id': 2, 'libelle': "Appartement"},
      'libelle': "Appartement moderne en ville",
      'image': "appartement.png",
      'adresse': "456 Main St, Ville, USA",
      'habitantsMax': 4,
      'chambres': 2,
      'lits': 4,
      'sdb': 1,
      'superficie': 100,
      'prixmois': 500,
      'options': [
        {'id': 3, 'libelle': "Gym", 'description': "Gym privé"},
        {'id': 4, 'libelle': "Piscine", 'description': "Piscine commune"},
      ],
      'optionPayantes': [
        {'id': 3, 'libelle': "Parking", 'description': "Parking souterrain", 'prix': 150},
        {'id': 4, 'libelle': "Cleaning", 'description': "Service de nettoyage", 'prix': 100},
      ],
    },
    {
      'id': 3,
      'typehabitat': {'id': 1, 'libelle': "Maison"},
      'libelle': "Maison de campagne spacieuse",
      'image': "maison.png",
      'adresse': "789 Country Rd, Campagne, USA",
      'habitantsMax': 6,
      'chambres': 3,
      'lits': 6,
      'sdb': 2,
      'superficie': 200,
      'prixmois': 800,
      'options': [
        {'id': 5, 'libelle': "Jardin", 'description': "Jardin privé"},
        {'id': 6, 'libelle': "BBQ", 'description': "Zone BBQ commune"},
      ],
      'optionPayantes': [
        {'id': 5, 'libelle': "Internet", 'description': "Internet haut débit", 'prix': 50},
        {'id': 6, 'libelle': "Télévision", 'description': "Télévision satellite", 'prix': 75},
      ],
    },
    {
      'id': 4,
      'typehabitat': {'id': 2, 'libelle': "Appartement"},
      'libelle': "Appartement en bord de mer",
      'image': "appartement.png",
      'adresse': "321 Ocean Ave, Mer, USA",
      'habitantsMax': 2,
      'chambres': 1,
      'lits': 2,
      'sdb': 1,
      'superficie': 50,
      'prixmois': 700,
      'options': [
        {'id': 7, 'libelle': "Plage", 'description': "Accès direct à la plage"},
        {'id': 8, 'libelle': "Piscine", 'description': "Piscine commune"},
      ],
      'optionPayantes': [
        {'id': 7, 'libelle': "Location de voiture", 'description': "Service de location de voiture", 'prix': 200},
        {'id': 8, 'libelle': "Service de conciergerie", 'description': "Service de conciergerie", 'prix': 150},
      ],
    },
    {
      'id': 5,
      'typehabitat': {'id': 2, 'libelle': "Appartement"},
      'libelle': "Appartement en centre-ville",
      'image': "appartement.png",
      'adresse': "123 Main St, Ville, USA",
      'habitantsMax': 4,
      'chambres': 2,
      'lits': 4,
      'sdb': 1,
      'superficie': 75,
      'prixmois': 600,
      'options': [
        {'id': 9, 'libelle': "Ascenseur", 'description': "Ascenseur privé"},
        {'id': 10, 'libelle': "Piscine", 'description': "Piscine commune"},
      ],
      'optionPayantes': [
        {'id': 9, 'libelle': "Parking", 'description': "Parking souterrain", 'prix': 100},
        {'id': 10, 'libelle': "Laverie", 'description': "Service de lavage", 'prix': 50},
      ],
    },
    {
      'id': 6,
      'typehabitat': {'id': 2, 'libelle': "Appartement"},
      'libelle': "Appartement design en ville",
      'image': "appartement.png",
      'adresse': "456 High St, Ville, USA",
      'habitantsMax': 3,
      'chambres': 1,
      'lits': 2,
      'sdb': 1,
      'superficie': 60,
      'prixmois': 500,
      'options': [
        {'id': 11, 'libelle': "Gym", 'description': "Gym privé"},
        {'id': 12, 'libelle': "Sauna", 'description': "Sauna commun"},
      ],
      'optionPayantes': [
        {'id': 11, 'libelle': "Internet", 'description': "Internet haut débit", 'prix': 75},
        {'id': 12, 'libelle': "Cleaning", 'description': "Service de nettoyage", 'prix': 100},
      ],

    },
  ];

  static List<Habitation> buildList() {
    List<Habitation> list = data.map((item) =>
    Habitation.fromJson(item)).toList();
    return list;
  }
}