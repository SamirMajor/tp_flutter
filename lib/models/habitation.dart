import 'package:tp_flutter/models/type_habitat.dart';

import 'dart:convert';

class Habitation {
  int id;
  TypeHabitat typeHabitat;
  String image;
  String libelle;
  String adresse;
  int chambres;
  int lits;
  int salleBains;
  int nbPersonnes;
  int superficie;
  num prixmois;
  List<Option> options;
  List<OptionPayante> optionPayantes;


  Habitation(
      this.id,
      this.typeHabitat,
      this.image,
      this.libelle,
      this.adresse,
      this.chambres,
      this.lits,
      this.salleBains,
      this.nbPersonnes,
      this.superficie,
      this.prixmois,
      {this.options = const [],
        this.optionPayantes = const [],}
      );

  Habitation.fromJson(Map<String, dynamic> json):
        id = json['id'],
        typeHabitat = TypeHabitat.fromJson(json['typehabitat']),
        libelle = json['libelle'],
        image = json['image'],
        adresse = json['adresse'],
        nbPersonnes = json['habitantsMax'],
        chambres = json['chambres'],
        lits = json['lits'],
        salleBains = json['sdb'],
        superficie = json['superficie'],
        prixmois = json['prixmois'],
        options = (json['options'] as List)
            .map((item) => Option.fromJson(item))
            .toList(),
        optionPayantes = (json['optionPayantes'] as List)
            .map((item) => OptionPayante.fromJson(item))
            .toList()
  ;

  Map<String, dynamic> toJson() => {
    'id': id,
    'typehabitat': typeHabitat.toJson(),
    'libelle': libelle,
    'image': image,
    'adresse': adresse,
    'habitantsMax': nbPersonnes,
    'chambres': chambres,
    'lits': lits,
    'sdb': salleBains,
    'superficie': superficie,
    'prixmois': prixmois,
    'options': options.map((e) => e.toJson()).toList(),
    'optionPayantes': optionPayantes.map((e) => e.toJson()).toList(),
  };

}

class Option {

  final int id;
  final String libelle;
  final String description;

  Option({
    required this.id,
    required this.libelle,
    this.description = ""
  });

  factory Option.fromJson(Map<String, dynamic> json) {
    return Option(
      id: json['id'],
      libelle: json['libelle'],
      description: json['description'] ?? "",
    );
  }

  Map<String, dynamic> toJson() => {
    'id': id,
    'libelle': libelle,
    'description': description
  };

}

class OptionPayante extends Option {

  final num prix;

  OptionPayante({
    required int id,
    required String libelle,
    String description = "",
    this.prix = 0.0
  }) : super(id: id, libelle: libelle, description: description);

  factory OptionPayante.fromJson(Map<String, dynamic> json) {
    return OptionPayante(
      id: json['id'],
      libelle: json['libelle'],
      description: json['description'] ?? "",
      prix: json['prix'] ?? 0.0,
    );
  }

  @override
  Map<String, dynamic> toJson() => {
    'id': id,
    'libelle': libelle,
    'description': description,
    'prix': prix,
  };

}

