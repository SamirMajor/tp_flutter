import 'package:tp_flutter/models/type_habitat.dart';

class TypeHabitatData {
  static final data = [
    {'id': 1, 'libelle': "Maison"},
    {'id': 2, 'libelle': "Appartement"},
  ];

  static List<TypeHabitat> buildList() {
    List<TypeHabitat> list = data.map((e) =>
        TypeHabitat.fromJson(e)).toList();
    return list;
  }
}
