import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:tp_flutter/models/habitation.dart';
import 'package:tp_flutter/share/location_style.dart';
import 'package:tp_flutter/share/location_text_style.dart';
import 'package:tp_flutter/views/share/habitation_features_widget.dart';
import 'package:tp_flutter/views/share/habitation_option.dart';

class HabitationDetails extends StatefulWidget {
  final Habitation _habitation;

  const HabitationDetails(this._habitation, {Key? key}) : super(key: key);

  @override
  State<HabitationDetails> createState() => _HabitationDetailsState();
}

class _HabitationDetailsState extends State<HabitationDetails> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget._habitation.libelle),
      ),
      body: ListView(
        padding: EdgeInsets.all(4.0),
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(20.0),
            child: Image.asset(
              'assets/images/locations/${widget._habitation.image}',
              fit: BoxFit.fitWidth,
              ),
            ),
            Container(
              margin: EdgeInsets.all(8.0),
              child: Text(widget._habitation.adresse),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                HabitationOption(Icons.group, "${widget._habitation.nbPersonnes} personnes"),
                HabitationOption(Icons.bed, "${widget._habitation.chambres} chambres"),
                HabitationOption(Icons.fit_screen, "${widget._habitation.superficie} m²")
              ],
            ),
            HabitationFeaturesWidget(widget._habitation),
            _buildItems(),
            _buildOptionsPayantes(),
            _buildRentButton(),
        ],
      ),
    );
  }

  _buildRentButton() {
    var format = NumberFormat("### €");

    return Container(
      decoration: BoxDecoration(
        color: LocationStyle.backgroundColorPurple,
        borderRadius: BorderRadius.circular(8.0),
      ),
      margin: EdgeInsets.all(8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            margin: EdgeInsets.symmetric(horizontal: 8.0),
            child: Text(
              format.format(widget._habitation.prixmois),
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 8.0),
            child: ElevatedButton(
              onPressed: (){
                print('Louer habitation');
              },
              child: Text('Louer'),
            ),
          ),
        ],
      ),
    );
  }

  _buildItems() {
    var width = (MediaQuery.of(context).size.width / 2) - 15;

    return Wrap(
      spacing: 2.0,
      children: Iterable.generate(
        widget._habitation.options.length,
          (i) => Container(

          ),
      ).toList(),
    );
  }

  Widget _buildOptions() {
    return Wrap(
      spacing: 2.0,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              "Titre",
              style: LocationTextStyle.subTitleboldTextStyle,
            ),
            ...widget._habitation.options.map((option) => Container(
              padding: const EdgeInsets.only(left: 15),
              margin: const EdgeInsets.all(2),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  const SizedBox(height: 5),
                  Text(
                    option.libelle,
                    style: const TextStyle(fontWeight: FontWeight.bold),
                  ),
                  const SizedBox(height: 5),
                  Text(
                    option.description,
                    style: LocationTextStyle.regularGreyTextStyle,
                  ),
                ],
              ),
            )).toList()
          ],
        ),
      ],
    );
  }


  Widget _buildOptionsPayantes() {
   return Container(
     margin: const EdgeInsets.only(left: 10, right: 10),
     child: Column(
       crossAxisAlignment: CrossAxisAlignment.start,
       children: [
         Container(
           height: 8,
           margin: const EdgeInsets.only(left: 10, right: 10),
           decoration: const BoxDecoration(
               border: Border(
                   bottom: BorderSide(
                       width: 1,
                       color: Colors.grey
                   )
               )
           ),
         ),
         const SizedBox(
           width: 8,
         ),
         Text(
             "Options Payantes",
             style: LocationTextStyle.subTitleboldTextStyle
         ),
         const SizedBox(height: 8),
         ...widget._habitation.optionPayantes.map((optionpayantes) => Container(
           padding: const EdgeInsets.only(left: 15),
           margin: const EdgeInsets.all(2),
           child: Column(
             crossAxisAlignment: CrossAxisAlignment.start,
             children: <Widget>[
               const SizedBox(height: 5),
               Text(
                 optionpayantes.libelle,
                 style: const TextStyle(fontWeight: FontWeight.bold),
               ),
               const SizedBox(height: 5),
               Text(
                 optionpayantes.description,
                 style: LocationTextStyle.regularGreyTextStyle,
               ),
             ],
           ),
         )).toList()
       ],
     ),
   );
 }

}



