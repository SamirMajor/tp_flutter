import 'dart:html';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:tp_flutter/services/location_service.dart';
import '../services/habitation_service.dart';
import 'bottom_navigation_bar_widget.dart';

class LocationList extends StatefulWidget {
  static String routeName = '/locations';
  const LocationList({Key? key}) : super(key: key);

  @override
  State<LocationList> createState() => _LocationListState();
}

class _LocationListState extends State<LocationList> {
  final LocationService service = LocationService();
  late Future<List<Location>> _locations;

  @override
  void initState() {
    super.initState();
    _locations = service.getLocations();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Liste des locations"),
      ),
      body: Center(
        child: Container(),
      ),
      bottomNavigationBar: BottomNavigationBarWidget(2),
    );
  }
  _buildInfo(Location location) {

  }

  _buildRow(Location location) {
    String formattedDateDebut = DateFormat.yMMMEd().format(location.dateDebut);
    String formattedDateFin = DateFormat.yMMMEd().format(location.dateFin);
    return Container(
      child: Column(
        children: [
          Row(
            children: [
              Expanded(
                flex: 3,
                child: ListTile(
                  title: Text(formattedDateDebut),
                  subtitle: Text(formattedDateFin),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

}